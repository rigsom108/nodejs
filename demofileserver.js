var http = require('http');
var fs = require('fs');
var url = require('url');


http.createServer(function (req, res){

    var url_object = url.parse(req.url);
    var file_name = '.'+ url_object.pathname;

    fs.readFile(file_name, function(err, data){
        if (err) {
            res.writeHead(404,{'Content-Type':'text/html'});
            res.write('Not Found');
        }
        res.writeHead(200, {'Content-Type':'text/html'});
        res.write(data);
        res.end();
    });
}).listen(8080)